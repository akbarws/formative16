package controller;

import java.util.ArrayList;
import java.util.Random;

import model.Person;
import model.Player;

public class PlayerController {

	private ArrayList<Person> listPerson = new ArrayList<Person>();
	private ArrayList<Player> listPlayer = new ArrayList<Player>();
	private int gameTimes = 0;

	public int getGameTimes() {
		return gameTimes;
	}

	public void setGameTimes(int gameTimes) {
		this.gameTimes = gameTimes;
	}

	public void setListPlayer(ArrayList<Player> listPlayer) {
		this.listPlayer = listPlayer;
	}

	public ArrayList<Player> getListPlayer() {
		return listPlayer;
	}
	
	public ArrayList<Person> getListPerson() {
		return listPerson;
	}

	public void setListPerson(ArrayList<Person> listPerson) {
		this.listPerson = listPerson;
	}
	
	public void addPersonPlayer(String personName) {
		Person newPerson = new Person(personName);
		this.getListPerson().add(newPerson);
		this.getListPlayer().add(new Player(newPerson));
	}

	public void startGame() {
		System.out.println("Arisan Started ...");
		this.setGameTimes(gameTimes+1);
		this.setRandomCode();
		int indexWinner = new Random().nextInt(this.getListPlayer().size());
		Player winnerPlayer = this.getListPlayer().get(indexWinner);
		this.getListPlayer().remove(indexWinner);
		System.out.println("Arisan turn "+this.getGameTimes());
		System.out.println("Winner Code : "+winnerPlayer.getRandom_code());
		System.out.println("Winner Name : "+winnerPlayer.getPerson().getName());
		System.out.println("##############################");
	}

	private void setRandomCode() {
		this.getListPlayer().forEach(player -> {
			int leftLimit = 97;
		    int rightLimit = 122;
		    int targetStringLength = 5;
		    Random random = new Random();
		    StringBuilder buffer = new StringBuilder(targetStringLength);
		    for (int i = 0; i < targetStringLength; i++) {
		        int randomLimitedInt = leftLimit + (int) 
		          (random.nextFloat() * (rightLimit - leftLimit + 1));
		        buffer.append((char) randomLimitedInt);
		    }
		    String generatedString = buffer.toString();
		    player.setRandom_code(generatedString);
		});
		System.out.println("Random Code Generated ...");
	}
}
