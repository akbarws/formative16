package model;

public class Player {

	private Person person;
	private String random_code;
	
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getRandom_code() {
		return random_code;
	}
	public void setRandom_code(String random_code) {
		this.random_code = random_code;
	}
	
	public Player(Person person) {
		this.setPerson(person);
	}
}
