package view;

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import controller.PlayerController;


@SpringBootApplication
public class Formative16Application {
	
	PlayerController playerController = new PlayerController();
	Logger log = Logger.getLogger("Formative16Application.class");
	
	private void printMenu(Scanner scan) {
		System.out.println("1. Add person");
		System.out.println("2. Get Winner");
		System.out.println("3. Exit");
		System.out.print("Masukkan pilihan : ");
	}

	private void addPerson(Scanner scan) {
		boolean isValid = false;
		do {
			System.out.print("Input name : ");
			String personName = scan.nextLine();
			String regexName = "^[a-zA-Z]+(?:[\\s.]?[a-zA-Z]+)*$";
			if(Pattern.matches(regexName, personName)) {
				isValid = true;
				playerController.addPersonPlayer(personName);;
				log.info("Person added "+personName);
			} else {
				System.out.println("Not Valid");
			}
		} while(!isValid);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Formative16Application.class, args);
		Scanner scan = new Scanner(System.in);
		Formative16Application main = new Formative16Application();
		boolean isExit = false;
//		main.playerController.addPersonPlayer("person 1");
//		main.playerController.addPersonPlayer("person 2");
//		main.playerController.addPersonPlayer("person 3");
//		main.playerController.addPersonPlayer("person 4");
//		main.playerController.addPersonPlayer("person 5");
//		main.playerController.addPersonPlayer("person 6");
//		main.playerController.addPersonPlayer("person 7");
//		main.playerController.addPersonPlayer("person 8");
//		main.playerController.addPersonPlayer("person 9");
//		main.playerController.addPersonPlayer("person 10");
		do {
			main.printMenu(scan);
			String userChoice = scan.nextLine();
			System.out.println("##############################");
			String regexMenu = "^[1-3]";
			if(Pattern.matches(regexMenu, userChoice)) {
				switch(userChoice) {
					case "1" :
						main.addPerson(scan);
						break;
					case "2" :
						if(main.playerController.getListPerson().size()<10) {
							System.out.println("Minimum 10 people.");
							System.out.println("Current people : "+main.playerController.getListPerson().size());
						} else {
							if(main.playerController.getGameTimes()==main.playerController.getListPerson().size()) {
								System.out.println("Everyone is winner");
								System.out.println("Application exit ...");
								isExit = true;
							} else {
								main.playerController.startGame();								
							}
						}
						break;
					default : isExit = true;
				}
			} else {
				System.out.println("Not Valid");
			}
		} while(!isExit);
	}

}
